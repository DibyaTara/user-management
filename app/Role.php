<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'role';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'role_name',
        'created_at',
        'updated_at'
    ];
    public  function UserRole(){
        return $this->belongsTo('App\UserRole','role_id','id');
    }
}
