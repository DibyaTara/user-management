<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Salary extends Model
{
    protected $table = 'salary';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'Amount',
        'increment',
        'decrement',
        'created_at',
        'updated_at'
    ];
}
