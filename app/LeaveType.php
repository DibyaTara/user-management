<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveType extends Model
{
    protected $table = 'leave_type';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'type'
    ];
}
