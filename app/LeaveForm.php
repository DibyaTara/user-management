<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LeaveForm extends Model
{
    protected $table = 'leave_form';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'subject',
        'message',
        'permission',
        'created_at',
        'updated_at',
        'Authorized_person',
        'leave_date'
    ];
    public  function leaveType(){
        return $this->hasMany('App\LeaveType','leave_type_id','id');
    }
}
