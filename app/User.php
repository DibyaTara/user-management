<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table='user';

    protected $fillable = [
        'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public  function userRole(){
        return $this->hasMany('App\UserRole','user_id','id');
    }
    public  function profile(){
        return $this->belongsTo('App\Profile','user_id','id');
    }
    public  function salary(){
        return $this->belongsTo('App\Salary','user_id','id');
    }
    public  function attendence(){
        return $this->belongsTo('App\Attendence','user_id','id');
    }
    public  function leaveForm(){
        return $this->hasMany('App\LeaveForm','user_id','id');
    }
}
