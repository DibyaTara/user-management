<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attendence extends Model
{
    protected $table = 'attendence';
    public $primaryKey = 'id';
    public $timestamps = false;
    protected $fillable = [
        'check_in',
        'check_out',
        'absent_days',
        'created_at',
        'updated_at'
    ];

    public  function leaveForm(){
        return $this->belongsTo('App\LeaveForm','attendence_id','id');
    }
}
