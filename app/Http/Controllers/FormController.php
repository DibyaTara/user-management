<?php

namespace App\Http\Controllers;

use App\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FormController extends Controller
{
    public function save(Request $request)
    {
        $data_img = $request->file('profile_image');

        if ($data_img->isValid()) {
            $ext = $data_img->getClientOriginalExtension();
            $path = public_path() . '/frontend/upload/';
            $filename = time() . '-' . $ext;
            $data_img->move($path, $filename);
        }

        $data = new Profile();
        $data->firstname = $request->input('firstname');
        $data->middlename = $request->input('middlename');
        $data->lastname = $request->input('lastname');
        $data->contactno = $request->input('contactno');
        $data->address = $request->input('address');
        $data->dob = $request->input('dob');
        $data->post = $request->input('post');
        $data->created_at = $request->input('created_at');
        $data->updated_at = $request->input('updated_at');

        $data->profile_image = $filename;

        /* print_r($data);die();*/
        $data->save();

        if ($data->save()) {
            return redirect()->route('stafflist')->with(['message' => 'Sorry !! Can not insert data']);
        } else {
            return view('pages.form');
        }
    }

    public function create()
    {
        return view('pages.form');
    }

    public function staffList()
    {
        $datas = Profile::all();

        return view('pages.staffList', compact('datas'));
    }

    public function editUser($id)
    {
        $datas = Profile::find($id);


        return view('pages.edit', compact('datas'))->with(['message' => 'your Data has been edited']);
    }

    public function updateUser(Request $request)
    {
        $datas = DB::table('profile')
            ->where('id', '=', $request->input('id'))
            ->update([
                'profile_image' => $request->input('profile_image'),
                'firstname' => $request->input('firstname'),
                'middlename' => $request->input('middlename'),
                'lastname' => $request->input('lastname'),
                'contactno' => $request->input('contactno'),
                'address' => $request->input('address'),
                'dob' => $request->input('dob'),
                'post' => $request->input('post')
        ]);

        return redirect('/stafflist')->with(['message' => 'your Data has been updated']);
    }

    public function deleteUser($id)
    {
        $datas = DB::table('profile')
            ->where('id', '=', $id)
            ->delete();
        return redirect()->route('stafflist');
        $data = DB::table('user')
            ->where('id', '=', $id)
            ->delete();
    }
}
