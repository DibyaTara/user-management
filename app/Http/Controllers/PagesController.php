<?php

namespace App\Http\Controllers;


use App\Profile;
use App\User;
use App\UserRole;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Auth;
use Mockery\Exception;

class PagesController extends Controller
{
    public function login()
    {
        return view('auth.login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }

    public function loginuser(Request $request)
    {
        /* echo ('dibbya');die();*/
        try {
            $email = $request->get('email');
            $password = $request->get('password');

            if (Auth::attempt(['email' => $email, 'password' => $password])) {

                return view('pages.index');
//                echo('i am here');
            } else {
                return view('auth.login');
            }
        } catch (\Exception $e) {
            return $e;
        }
    }

    public function profilePage()
    {
        return view('pages.profilePage');
    }

    public function form()
    {
        return view('pages.form');
    }

    public function showForm()
    {
        return view('auth.register');
    }

    public function insertUser(Request $request)
    {
        $user = User::create([
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password'))
        ]);

        $user_id = $user->id;

        $userRole = UserRole::create([
            'user_id' => $user_id,
            'role_id' => 2
        ]);

        $user = Profile::create([

            'user_id' => $user_id,
            'firstname' => $request->get('firstname'),
            'middlename' => $request->input('middlename'),
            'lastname' => $request->input('lastname'),
            'contactno' => $request->input('contactno'),
            'address' => $request->input('address'),
            'dob' => $request->input('dob'),
            'post' => $request->input('post')
        ]);

        $user->save();

        if ($user->save()) {
            return redirect('login')->with(['message' => 'welcome to hotel']);
        } else {
            return view('pages.form');
        }
    }
}
