<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $table = 'profile';
    public $primaryKey = 'id';
    public $timestamps = true;
    protected $fillable = [
        'user_id',
        'profile_image',
        'firstname',
        'middlename',
        'lastname',
        'contactno',
        'address',
        'dob',
        'post',
        'created_at',
        'updated_at'
    ];
    public  function User(){
        return $this->belongsTo('App\User','user_id','id');
    }

}
