<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

/* mileko */
Route::get('/','PagesController@login');
Route::post('/loginUser', ['uses' =>'PagesController@loginUser', 'as' => 'loginuser']);
Route::get('/registerUser', ['uses' =>'PagesController@showForm', 'as' => 'showForm']);
Route::post('/registerUser', ['uses' =>'PagesController@insertUser', 'as' => 'insertUser']);
Route::get('/logout',['uses'=>'PagesController@logout','as'=>'logout']);

/* baki */
Route::get('/profilePage','PagesController@profilePage');
Route::get('/form','PagesController@form');
Route::get('/index','PagesController@index');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/register', ['uses' => 'FormController@create', 'as' => 'pages.form']);
Route::post('/form', ['uses' => 'FormController@save', 'as' => 'form']);
Route::get('/stafflist', ['uses' => 'FormController@staffList', 'as' => 'stafflist']);
Route::get('/edit/{id}', ['uses' => 'FormController@editUser', 'as' => 'editUser']);
Route::post('/update', ['uses' => 'FormController@updateUser', 'as' => 'updateuser']);
Route::get('/delete/{id}', ['uses' => 'FormController@deleteUser', 'as' => 'deleteuser']);
