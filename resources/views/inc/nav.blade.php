<header class="header dark-bg">
    <div class="toggle-nav">
        <div class="icon-reorder tooltips" data-original-title="Toggle Navigation" data-placement="bottom"><i
                    class="icon_menu"></i></div>
    </div>
    <!--logo start-->
    <a href="{{asset('pages/index')}}" class="logo">HO <span class="lite">TEL</span></a>
    <!--logo end-->

    <div class="nav search-row" id="top_menu">
        <!--  search form start -->
        <ul class="nav top-menu">
            <li>
                <form class="navbar-form">
                    <input class="form-control" placeholder="Search" type="text">
                </form>
            </li>
        </ul>
        <!--  search form end -->
    </div>

    <div class="top-nav notification-row">
        <!-- notificatoin dropdown start-->
        <ul class="nav pull-right top-menu">
            <!-- user login dropdown start-->
            <div class="nav profile-nav">
                <a href="/profilePage"><i class="icon_profile"></i> My Profile</a>
            </div>
            <div class="nav logout" id="top_menu">
                <a href="{{route('logout')}}"><i class="icon_key_alt"></i> LogOut</a>
            </div>
        </ul>
        <!-- user login dropdown end -->
    </div>
</header>
<!--header end-->