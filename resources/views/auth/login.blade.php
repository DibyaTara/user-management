@extends('layouts.main')

@section('content')
    <div class="login-img3-body">
        <div class="wrapper">

            <form class="login-form" action="{{route('loginuser')}}" method="POST">
                <div class="login-wrap">
                    <p class="login-img"><i class="icon_lock_alt"></i></p>
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon_profile"></i></span>
                            <input id="name" type="text" class="form-control" name="email" required>
                            @if ($errors->has('email'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="icon_key_alt"></i></span>
                            <input id="password" type="password" class="form-control" name="password" required>
                            @if ($errors->has('password'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <label class="checkbox">
                        <input type="checkbox" value="remember-me {{ old('remember') ? 'checked' : '' }}"> Remember me
                        <span class="pull-right"> <a href="{{ route('password.request')}}"> Forgot Password?</a></span>
                    </label>
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Login</button>

                </div>
                <input type="hidden" name="_token" value="{{csrf_token()}}">
            </form>
            <div class="form-group">
                <button class="btn btn-info btn-lg btn-block-center">
                    <a href="{{url('/registerUser')}}">Signup</a>
                </button>
            </div>
        </div>
    </div>
@endsection