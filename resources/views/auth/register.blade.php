@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Register</div>
                    <div class="panel-body">
                        <form class="form-horizontal" method="POST" action="{{ route('insertUser') }}"
                              enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email"
                                           value="{{ old('email') }}" required>

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Password</label>

                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required>

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                           name="password_confirmation" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="profile" class="col-md-4 control-label">Profile</label>

                                <div class="col-md-6">
                                    <input type="file" name="profile_image" onchange="readURL(this);" required/>
                                    <img id="img" src="#" alt="upload youyr Image">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="firstname" class="col-md-4 control-label">First Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="firstname" name="firstname" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="middlename" class="col-md-4 control-label">Middle Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="middlename" name="middlename" type="text" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="lastname" class="col-md-4 control-label">Last Name</label>
                                <div class="col-md-6">
                                    <input class="form-control" id="lastname" name="lastname"
                                           type="text" required/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="contact" class="col-md-4 control-label">Contact No.</label>
                                <div class="col-md-6">
                                    <input class="form-control " id="contactno" type="text" name="contactno"
                                           required/>
                                </div>
                            </div>
                                <div class="form-group">
                                    <label for="address" class="col-md-4 control-label">Address</label>
                                    <div class="col-md-6">
                                        <input class="form-control" id="address" name="address" type="text" required/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="dob" class="col-md-4 control-label">D.O.B</label>
                                    <div class="col-md-6">
                                        <input class="form-control" id="dob" name="dob" type="text" required/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="post" class="col-md-4 control-label">Post</label>
                                    <div class="col-md-6">
                                        <input class="form-control" id="post" name="post" type="text" required/>
                                    </div>
                                </div>

                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Register
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
