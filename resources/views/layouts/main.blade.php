<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Creative - Bootstrap 3 Responsive Admin Template">
    <meta name="author" content="GeeksLabs">
    <meta name="keyword" content="Creative, Dashboard, Admin, Template, Theme, Bootstrap, Responsive, Retina, Minimal">
    <link rel="shortcut icon" href="{{asset('frontend/img/favicon.png')}}">

    <title>Hotel</title>

    <!-- Bootstrap CSS -->
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{asset('frontend/css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{asset('frontend/css/elegant-icons-style.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/css/font-awesome.css')}}" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style-responsive.css')}}" rel="stylesheet"/>
    <!-- Bootstrap CSS -->
    <link href="{{asset('frontend/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- bootstrap theme -->
    <link href="{{asset('frontend/css/bootstrap-theme.css')}}" rel="stylesheet">
    <!--external css-->
    <!-- font icon -->
    <link href="{{asset('frontend/css/elegant-icons-style.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/css/font-awesome.min.css')}}" rel="stylesheet"/>
    <!-- Custom styles -->
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style-responsive.css')}}" rel="stylesheet"/>
    <!-- full calendar css-->
    <link href="{{asset('frontend/assets/fullcalendar/fullcalendar/bootstrap-fullcalendar.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/assets/fullcalendar/fullcalendar/fullcalendar.css')}}" rel="stylesheet"/>
    <!-- easy pie chart-->
    <link href="{{asset('frontend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css')}}" rel="stylesheet"
          type="text/css" media="screen"/>
    <!-- owl carousel -->
    <link rel="stylesheet" href="{{asset('frontend/css/owl.carousel.css')}}" type="text/css">
    <link href="{{asset('frontend/css/jquery-jvectormap-1.2.2.css')}}" rel="stylesheet">
    <!-- Custom styles -->
    <link rel="stylesheet" href="{{asset('frontend/css/fullcalendar.css')}}">
    <link href="{{asset('frontend/css/widgets.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('frontend/css/style-responsive.css')}}" rel="stylesheet"/>
    <link href="{{asset('frontend/css/xcharts.min.css')}}" rel=" stylesheet">
    <link href="{{asset('frontend/css/jquery-ui-1.10.4.min.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 -->
    <!--[if lt IE 9]>
    <script src="{{asset('frontend/js/html5shiv.js')}}"></script>
    <script src="{{asset('frontend/js/respond.min.js')}}"></script>
    <![endif]-->
</head>
<body>
<!-- container section start -->
<section id="container" class="">
   @if(Auth::check())
    <header>
        @include('inc.nav')
        @include('inc.sideBar')
    </header>
    @endif

    @yield('content')
</section>
<!-- container section end -->
<!-- javascripts -->
<script src="{{asset('frontend/js/jquery.js')}}"></script>
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- nice scroll -->
<script src="{{asset('frontend/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<!-- jquery knob -->
<script src="{{asset('frontend/assets/jquery-knob/js/jquery.knob.js')}}"></script>
<!--custome script for all page-->
<script src="{{asset('frontend/js/scripts.js')}}"></script>

<script>

    //knob
    $(".knob").knob();

</script>
<!-- javascripts -->
<script src="{{asset('frontend/js/jquery.js')}}"></script>
<script src="{{asset('frontend/js/jquery-ui-1.10.4.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery-1.8.3.min.js')}}"></script>
<script type="text/javascript" src="{{asset('frontend/js/jquery-ui-1.9.2.custom.min.js')}}"></script>
<!-- bootstrap -->
<script src="{{asset('frontend/js/bootstrap.min.js')}}"></script>
<!-- nice scroll -->
<script src="{{asset('frontend/js/jquery.scrollTo.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.nicescroll.js')}}" type="text/javascript"></script>
<!-- charts scripts -->
<script src="{{asset('frontend/assets/jquery-knob/js/jquery.knob.js')}}"></script>
<script src="{{asset('frontend/js/jquery.sparkline.js')}}" type="text/javascript"></script>
<script src="{{asset('frontend/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js')}}"></script>
<script src="{{asset('frontend/js/owl.carousel.js')}}"></script>
<!-- jQuery full calendar -->
<
<script src="{{asset('frontend/js/fullcalendar.min.js')}}"></script> <!-- Full Google Calendar - Calendar -->
<script src="{{asset('frontend/assets/fullcalendar/fullcalendar/fullcalendar.js')}}"></script>
<!--script for this page only-->
<script src="{{asset('frontend/js/calendar-custom.js')}}"></script>
<script src="{{asset('frontend/js/jquery.rateit.min.js')}}"></script>
<!-- custom select -->
<script src="{{asset('frontend/js/jquery.customSelect.min.js')}}"></script>
<script src="{{asset('frontend/assets/chart-master/Chart.js')}}"></script>

<!--custome script for all page-->
<script src="{{asset('frontend/js/scripts.js')}}"></script>
<!-- custom script for this page-->
<script src="{{asset('frontend/js/sparkline-chart.js')}}"></script>
<script src="{{asset('frontend/js/easy-pie-chart.js')}}"></script>
<script src="{{asset('frontend/js/jquery-jvectormap-1.2.2.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery-jvectormap-world-mill-en.js')}}"></script>
<script src="{{asset('frontend/js/xcharts.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.autosize.min.js')}}"></script>
<script src="{{asset('frontend/js/jquery.placeholder.min.js')}}"></script>
<script src="{{asset('frontend/js/gdp-data.js')}}"></script>
<script src="{{asset('frontend/js/morris.min.js')}}"></script>
<script src="{{asset('frontend/js/sparklines.js')}}"></script>
<script src="{{asset('frontend/js/charts.js')}}"></script>
<script src="{{asset('frontend/js/jquery.slimscroll.min.js')}}"></script>
<script>

    //knob
    $(function () {
        $(".knob").knob({
            'draw': function () {
                $(this.i).val(this.cv + '%')
            }
        })
    });

    //carousel
    $(document).ready(function () {
        $("#owl-slider").owlCarousel({
            navigation: true,
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true

        });
    });

    //custom select box

    $(function () {
        $('select.styled').customSelect();
    });

    /* ---------- Map ---------- */
    $(function () {
        $('#map').vectorMap({
            map: 'world_mill_en',
            series: {
                regions: [{
                    values: gdpData,
                    scale: ['#000', '#000'],
                    normalizeFunction: 'polynomial'
                }]
            },
            backgroundColor: '#eef3f7',
            onLabelShow: function (e, el, code) {
                el.html(el.html() + ' (GDP - ' + gdpData[code] + ')');
            }
        });
    });

</script>
</body>
</html>