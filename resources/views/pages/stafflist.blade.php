@extends('layouts.main')

@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <div class="well">
                    <h1><p class="text-center">List of Users</p></h1>
                </div>
                <table class="table table-condensed">
                    <tr>
                        <th>S.No.</th>
                        <th>Profile</th>
                        <th>First Name</th>
                        <th>Middle Name</th>
                        <th>Last Name</th>
                        <th>Contact No.</th>
                        <th>Address</th>
                        <th>Date Of Birth</th>
                        <th>Post</th>
                        <th>Action</th>
                    </tr>
                    @if($datas)
                        <?php $i = 0 ?>
                        @foreach($datas as $data)
                            <?php $i++ ?>
                            <tr>

                                <td>{{$i }}</td>
                                <td>{{ $data['profile_image']}}</td>
                                <td>{{ $data['firstname'] }}</td>
                                <td>{{ $data['middlename'] }}</td>
                                <td>{{ $data['lastname'] }}</td>
                                <td>{{ $data['contactno'] }}</td>
                                <td>{{ $data['address'] }}</td>
                                <td>{{ $data['dob'] }}</td>
                                <td>{{ $data['post'] }}</td>
                                <td>
                                    <a href="{{ route('editUser', ['form_id' => $data->id]) }}" class="btn btn-success"
                                       name="Edit">Edit</a>|
                                    <a href="{{ route('deleteuser', ['form_id' => $data->id]) }}" class="btn btn-danger"
                                       name="Delete">Delete</a>
                                </td>
                            </tr>
                        @endforeach
                    @else
                        <tr>
                            No Data available
                        </tr>
                    @endif
                </table>

                <a href="{{ route('pages.form') }}" class="btn btn-warning"> Add New User</a>

            </div>
        </section>
    </section>

@endsection