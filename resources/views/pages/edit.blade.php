@extends('layouts.main')
@section('content')
    <section id="main-content">
        <section class="wrapper">
            <div class="container">
                <h1><p class="text-center">Application form!!</p></h1>

            </div>
            <hr>
            <div class="container">
                @if($datas)
                    <form class="form-horizontal" action="{{ route('updateuser') }}" method="post" enctype="multipart/form-data">
                        <p class="text-center">
                        <table class="table table-condensed">

                            {{--<div class="container-fluid">--}}
                                {{--<label for="profile_image">Profile </label> <input type="image" class="form-control"--}}
                                                                                   {{--name="profile_image"--}}
                                                                                   {{--value="{{ $datas->profile_image }}">--}}
                            {{--</div>--}}
                            {{--<br>--}}
                            <div class="container-fluid">
                                <label for="firstname">First Name</label> <input type="text" class="form-control" name="firstname" value="{{ $datas->firstname }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="middlename">Middle Name</label> <input type="text" class="form-control" name="middlename" value="{{ $datas->middlename }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="lastname">Last Name</label> <input type="text" class="form-control" name="lastname" value="{{ $datas->lastname }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="contactno">Contact No.</label> <input type="text" class="form-control" name="contactno" value="{{ $datas->contactno }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="address">Address</label> <input type="text" class="form-control" name="address" value="{{ $datas->address }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="dob">Date Of Birth</label> <input type="text" class="form-control" name="dob"
                                                                              value="{{ $datas->dob }}">
                            </div>
                            <br>
                            <div class="container-fluid">
                                <label for="post">Post</label> <input type="text" class="form-control" name="post"
                                                                      value="{{ $datas->post }}">
                            </div>
                            <br>
                            <input type="Submit" class="btn btn-primary" name="Submit">
                            <input type="hidden" name="id" value="{{ $datas->id }}">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">

                        </table>
                        </p>
                    </form>
                @endif
            </div>
        </section>
    </section>
@endsection