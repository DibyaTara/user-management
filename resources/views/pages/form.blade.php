@extends('layouts.main')
@section('content')
    <!--main content start-->
    <section id="main-content">
        <section class="wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header"><i class="fa fa-files-o"></i> Form </h3>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-home"></i><a href="">Home</a></li>
                        <li><i class="fa fa-files-o"></i>Form</li>
                    </ol>
                </div>
            </div>
            <!-- Form validations -->
            <div class="row">
                <div class="col-lg-12">
                    <section class="panel">
                        <header class="panel-heading">
                            Form validations
                        </header>
                        <div class="panel-body">
                            <script>
                                function readURL(input) {
                                    if (input.file && input.file[0]) {
                                        var reader = new fileReader();
                                        reader.onload = function (e) {
                                            $('#img')
                                                .attr('src', e.target.result)
                                                .width(200)
                                                .height(150);
                                        };
                                        reader.readAsDataURL(input.file[0]);
                                    }
                                }
                            </script>
                            <div class="form">
                                <form class="form-validate form-horizontal" id="feedback_form" method="post" action="{{ route('form') }}" enctype="multipart/form-data">
                                    <div class="form-group ">
                                        <label for="profile_image" class="control-label col-lg-2"> Profile </label>
                                        <div class="col-lg-10">
                                            <input type="file" name="profile_image" onchange="readURL(this);"  required/>
                                            <img id="img" src="#" alt="upload youyr Image">
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="firstname" class="control-label col-lg-2">First Name <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="firstname" name="firstname" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="middlename" class="control-label col-lg-2">Middle Name <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="middlename" name="middlename" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="lastname" class="control-label col-lg-2">Last Name <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="lastname" name="lastname"
                                                   type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="contactno" class="control-label col-lg-2">Contact No.<span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control " id="contactno" type="text" name="contactno"
                                                   required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="address" class="control-label col-lg-2">Address <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="address" name="address" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="dob" class="control-label col-lg-2">D.O.B. <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="dob" name="dob" type="text" required/>
                                        </div>
                                    </div>
                                    <div class="form-group ">
                                        <label for="post" class="control-label col-lg-2">Post <span
                                                    class="required">*</span></label>
                                        <div class="col-lg-10">
                                            <input class="form-control" id="post" name="post" type="text" required/>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-lg-offset-2 col-lg-10">
                                            <button class="btn btn-primary" type="submit">Save</button>
                                            <button class="btn btn-default" type="button">Cancel</button>
                                        </div>
                                    </div>
                                  <input type="hidden" name="_token" value=" {{csrf_token()}}">
                                </form>
                            </div>

                        </div>
                    </section>
                </div>
            </div>
        </section>
    </section>
@endsection